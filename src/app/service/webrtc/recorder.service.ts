import { Injectable } from '@angular/core';

@Injectable()
export class RecorderService {

    private Stream: any = null;
    private recorder: MediaRecorder;
    private recordData = [];
    private recordeTarget: any;
    private recordeURL: any;

    private Options = {
        videoBitsPerSecond : 512000,
        mimeType : 'video/webm; codecs=vp9'
    };
    constructor() {}

    /**
     * 録画用の映像ストリームを渡す
     * @param stream
     */
    public setStream(stream): this {
        this.Stream = stream;
        return this;
    }

    /**
     * 録画映像の再生先登録
     * @param target 録画再生ターゲットDOM
     */
    public setRecordePlayer(target): this {
        this.recordeTarget = target;
        return this;
    }

    /**
     * 録画時のビットレートコーデックの設定
     * @param options
     */
    public setRecordeOptions(options): this {
        // ビットレート設定
        this.Options.videoBitsPerSecond =
            ('videoBitsPerSecond' in options)
                ? options['videoBitsPerSecond']
                : this.Options.videoBitsPerSecond;
        // コーデック設定
        this.Options.mimeType =
        ('mimeType' in options)
            ? options['mimeType']
            : this.Options.mimeType;
        return this;
    }
    /**
     * 録画データのDL用URL取得
     */
    public getRecordeURL(): string {
        return this.recordeURL;
    }

    /**
     * 録画開始
     * @param time 録画時間
     * デフォルトで録画時間は10秒
     * コーデックはvp9
     * ビットレート　512kにて録画される
     */
    public startRecord(time: number = 1000): this {
        this.recorder = new MediaRecorder(this.Stream, this.Options);
        this.recorder.ondataavailable = (result) => {
            this.recordData.push(result.data);
        };
        this.recorder.start(time);
        return this;
    }
    /**
     * 録画停止
     */
    public stopRecord(): this {
        this.recorder.onstop =  (result) => {
            this.recorder = null;
        };
        this.recorder.stop();
        return this;
    }

    /**
     * 録画の再生
     * @param number record 録画番号
     */
    public plyaRecord(record: number = 0): this {
        if (this.recordeTarget === null) {
            console.error('Recorde Player Not Set');
            return;
        }
        const videoBlob = new Blob(
                this.recordData[record],
                { type: 'video/webm'}
            );
        this.recordeURL = window.URL.createObjectURL(videoBlob);

        // 既にURLオブジェクトが生成済みの場合、一旦削除する
        if (this.recordeTarget.src) {
            window.URL.revokeObjectURL(this.recordeTarget.src);
            this.recordeTarget.src = null;
        }
        this.recordeTarget.src = this.recordeURL;
        this.recordeTarget.play();

        return this;
    }

}
