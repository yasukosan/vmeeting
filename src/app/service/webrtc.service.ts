import { Injectable } from '@angular/core';
import {
    RecorderService, SDPService,
    PearService
} from './';
import { SupportService } from './webrtc/support.service';

@Injectable()
export class WebRTCService {

    private LocalVideoTarget: any;
    private RemoteVideoTarget: any = {};
    private ContributeVideoTarget: any = null;

    private LocalStream: any = null;
    private RemoteStream: any = {};
    private ContributeStream: any = null;

    private SendDataConnect: any = {};
    // 配信モード 配信：contributor 音だけ：audio　受信：listener
    private videoMode: string = 'listener';

    constructor(
        private recordeService: RecorderService,
        private pearService: PearService,
        private sdpService: SDPService,
        private supportService: SupportService
    ) { }

    /**
     * 録画開始
     * @param number time 録画時間
     * 
     * @param string target 録画ソース（local, remote）
     */
    public startRecord(time: number = 1000, target: string = 'local'): void {
        if (target === 'local') {
            this.recordeService
            .setStream(this.LocalStream)
            .startRecord(time);
        } else {
            this.recordeService
            .setStream(this.RemoteStream)
            .startRecord(time);
        }
    }
    /**
     * 録画停止
     */
    public stopRecord(): void {
        this.recordeService.stopRecord();
    }
    /**
     * 録画映像の再生先登録
     * @param target 録画再生ターゲットDOM
     */
    public setRecordePlayer(target): void {
        this.recordeService.setRecordePlayer(target);
    }
    /**
     * 録画の再生
     * @param any target 録画映像を再生するターゲット
     */
    public plyaRecord(target): void {
        this.recordeService
            .setRecordeOptions(target)
            .plyaRecord();
    }

    /**
     * 録画データのDL用URL取得
     * @return string DL用URL
    */
    public getRecordeURL(): string {
        return this.recordeService.getRecordeURL();
    }

    /**
     * ローカルストリームの取得
     * @param any vmode ビデオの有無
     * @param boolean amode　オーディオの有無
     * @return Promise<boolean> ローカルストリームの取得成否
     */
    public getLocalStream(vmode: any = null, amode: boolean = false): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // 配信モードを確認
            const mode = this.checkStreamMode(vmode, amode);
            if (!vmode && !amode) {
                reject(false);
            }
            // mediaDeviceの取得オプション文字列取得
            const API = this.supportService.getMediAPI();
            console.log(API);
            // メディアデバイスに接続
            navigator.mediaDevices[API]({
                video: vmode,
                // video: {facingMode: 'user'},
                audio: mode.audio
            }).then((stream: MediaStream) => {
                // this.localStream = stream;
                console.log('Set Local Stream');
                this.setStream('local', stream);
                resolve(true);
            }).catch((error) => {
                console.error(error);
                reject(false);
            });
        });
    }

    /**
     * ランダムな文字列を生成
     * @param number len 文字列の長さ
     * @param string charSet 使用する文字一覧
     * @return string
     */
    public getRandomString(len: number, charSet: string = null): string {
        charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let randomString = '';
        for (let i = 0; i < len; i++) {
            const randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
        }
        return randomString;
    }

    /**
     * ランダムな番号を生成
     * @param number len 数字の長さ
     * @param string charSet 使用する数値一覧
     * @return number 
     */
    public getRandomNumber(len: number , charSet: string = null): number {
        charSet = charSet || '0123456789';
        let randomString = '';
        for (let i = 0; i < len; i++) {
            const randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
        }
        return Number(randomString);
    }

    /**
     * 配信モードの確認
     * @param vmode 
     * @param amode 
     * @return any
     */
    private checkStreamMode(vmode, amode): any {
        let v = null;
        const a = amode;
        if (typeof (vmode) === 'boolean' || vmode === null) {
            if (vmode === null) {
                v = true;
            } else {
                v = vmode;
            }
        } else {
            v = {
                mediaSource: vmode
            };
        }
        return { video: v, audio: a };
    }

    /**
     * websocketIDが登録済み
     * もしくは接続上限か確認
     * @param id websocketのセッションID
     */
    public checkAuthConnection(id): boolean {
        return this.pearService.checkAuthConnection(id);
    }

    /**
     * webrtcの接続モードが設定とあっているか確認
     * @param check_mode webrtcの接続モード
     * 接続モード
     * listener：受信専用
     * contributor：配信専用
     */
    public checkMode(check_mode): boolean {
        if (check_mode.includes(this.videoMode)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * GetUserMediaに対応しているか
     */
    public checkScreenShare(): boolean {
        return (this.supportService.getMediAPI() === null) ? false : true;
    }

    /**
     * MediaDeviceが利用可能か
     */
    public async checkMediaDevice(pathMode: boolean = false): Promise<object> {
        return this.supportService
            .checkScreenShare(pathMode)
            .checkMediaDevice()
            .then((result) => {
                return result;
            });
    }

    /**
     * ビデオタグのDOMオブジェクトを登録
     * @param local ローカル用ビデオタグ
     */
    public setVideoTarget(local): void {
        this.LocalVideoTarget = local;
    }
    /**
     * リモートビデオを表示するDOMオブジェクトのIDを登録
     * @param element
     * @param id 
     */
    public setRemoteVideoTarget(element, id): void {
        this.RemoteVideoTarget[id] = element;
    }
    /**
     * 
     * @param element 
     */
    public setContributorTarget(element): void {
        this.ContributeVideoTarget = element;
        console.log(this.ContributeVideoTarget);
    }
    public setVideoMode(mode): void {
        this.videoMode = mode;
    }
    public getVideoMode(): string {
        return this.videoMode;
    }


    /**
     * ストリームデータを変数に格納
     * @param target ローカルかリモートか
     * @param stream 映像ストリーム
     */
    public setStream(target, stream): void {
        if (target === 'local') {
            this.LocalStream = stream;
        } else if (target === 'remote') {
            // 現在未使用
            // this.RemoteStream = stream;
        }
    }

    /**
     * ビデオ再生
     * @param element ローカル映像かリモート映像か
     * @param id クライアントID
     */
    public playVideo(element, id = 0) {
        let videoTarget: any = '';
        if (element === 'local') {
            videoTarget = this.LocalVideoTarget;
            videoTarget.srcObject = this.LocalStream;
            videoTarget.volume = 0;
        } else if (element === 'contributor') {
            videoTarget = this.ContributeVideoTarget;
            videoTarget.srcObject = this.ContributeStream;
        } else if (element === 'remote') {
            // 自身が配信者、相互通信中の場合
            videoTarget = this.RemoteVideoTarget[id];
            videoTarget.srcObject = this.RemoteStream[id];
        }
        // ストリーム再生
        videoTarget.play();
    }

    /**
     * データチャンネルでデータ送信
     * @param data 送信データ
     */
    public sendData(data): boolean {
        return this.pearService.sendData(data);
    }

    /**
     * PeerConnection新規作成
     * @param id クライアントID
     */
    private NewPearConnection(id): any {
        const peer: any = this.pearService.initPearConnection(id);
        if ('ontrack' in peer) {
            peer.ontrack = (event) => {
                if (this.checkMode(['audio', 'listener'])) {
                    this.ContributeStream = event.streams[0];
                    this.playVideo('contributor');
                } else {
                    this.RemoteStream[id] = event.streams[0];
                    this.playVideo('remote', id);
                }
            };
        }
        // ローカルストリームの追加
        if (this.LocalStream) {
            // if (this.checkMode(['contributor'])) {
            console.log('Add local stream');
            peer.addStream(this.LocalStream);
            // }
        } else {
            console.warn('no local stream');
        }
        return peer;
    }

    /**
     * websocketからイベント受け取り
     * @param tag イベント名
     */
    public onSdpText(sdp: any, id: any): void {
        // オファーの受け取り
        if (sdp['type'] === 'offer') {
            if (this.checkMode(['contributor', 'audio', 'listener'])) {
                console.log('Receive offer');
                sdp = this.sdpStripper(sdp);
                const offer = new RTCSessionDescription(sdp);
                const peer = this.NewPearConnection(id);
                this.pearService.setOffer(offer, id, peer);
            }
        // アンサーの受け取り
        } else if (sdp['type'] === 'answer') {
            console.log('Receive answer');
            if (this.checkMode(['contributor', 'audio'])) {
                console.log('Receive answer');
                const answer = new RTCSessionDescription(sdp);
                this.pearService.setAnswer(answer, id);
            }
        // アイスの受け取り
        } else if (sdp['type'] === 'candidate') {
            console.log('Received ICE candidate');
            const candidate = new RTCIceCandidate(sdp['data']);
            this.pearService.setIceCandidate(candidate, id);
        }
    }

    /**
     * SDP情報から不要なコーデック情報を削除し再パッケージする
     * @param sdp SDP
     * 現在固定機能で「VP8」「VP9」を削除
     */
    private sdpStripper(sdp): object {
        return this.sdpService.sdpStripper(sdp);
    }

    /**
     * オファー作成
     * peerconnectionの作成とイベントを登録し
     * 接続待受の準備が出来た後にオファー情報を作成
     */
    public makeOffer(id): void {
        const options = {};
        /*
        if (this.videoMode === 'contributor') {
            console.log(this.videoMode);
            options = {
                mandatory: {
                'OfferToReceiveAudio': true,
                'OfferToReceiveVideo': false
                }
            };
        }*/
        const peer = this.NewPearConnection(id);
        this.pearService.makeOffer(id, options, peer);
    }

    /**
     * WebRTCコネクション切断
     */
    public hungUp(): void {
        this.pearService.hungUp();
    }


}
